#include "database.h"

#include "json_helper.h"


using namespace emumba::training;
using json = nlohmann::json;

//json related functions
bool database::parsedata(json _db_list) // takes data  in json data type and creates and fills map
{
    for (auto &&person : _db_list)
    {
        add_person(person);
    }
    return true;
}

//query related
json database::query_function(const json &_query_obj)
{
    std::string query_key = _query_obj.begin().key();
    if (query_key == "name")
    {
        auto &&map_iter = personal_info_map.find(_query_obj[query_key]);
        if (map_iter != personal_info_map.end())
        {
            return json{*(map_iter->second)};
        }
    }
    if (query_key == "account id")
    {
        auto &&map_iter = account_info_map.find(_query_obj[query_key]);
        if (map_iter != account_info_map.end())
        {
            return json{*(map_iter->second)};
        }
    }

    json ret_json;

    if (is_person_attr(query_key))
    {
        person_query(ret_json, _query_obj);
    }
    if (is_acct_attr(query_key))
    {
        acct_query(ret_json, _query_obj);
    }
    return ret_json;
}
void database::person_query(json &ret_json, const json &_query)
{
    auto &&iter = personal_info_map.begin();
    while (iter != personal_info_map.end())
    {
        iter = std::find_if(iter, personal_info_map.end(), [_query](per_pair A)
                            { return does_contains(A, _query); });
        if (iter != personal_info_map.end())
        {
            ret_json.push_back(*(iter->second));
            ++iter;
        }
    }
}
void database::acct_query(json &ret_json, const json &_query)
{
    auto &&iter = account_info_map.begin();
    while (iter != account_info_map.end())
    {
        iter = std::find_if(iter, account_info_map.end(), [_query](acct_pair A)
                            { return does_contains(A, _query); });
        if (iter != account_info_map.end())
        {
            ret_json.push_back(*(iter->second));
            ++iter;
        }
    }
}

//person
bool database::add_person(personal_data _person)
{
    //check if personal_data is valid
    auto map_ptr = personal_info_map.find(_person.get_name());
    if (map_ptr == personal_info_map.end())
    {
        personal_info.push_back(_person);
        personal_info_map[_person.get_name()] = &personal_info.back();
    }
    else
    {
        map_ptr->second->set_data(_person.get_data());
    }
    return true;
}
bool database::add_person(const json &_person)
{
    personal_data_struct person_struct;

    try
    {
        if (!is_person(_person))
        {
            return false;
        }
        person_struct.age = _person["age"];
        person_struct.city = _person["city"];
        person_struct.coordinates[LATI] = _person["coordinates"]["lat"];
        person_struct.coordinates[LONG] = _person["coordinates"]["long"];

        for (auto &&acct : _person["accounts"])
        {
            add_account(_person["name"], acct);
        }
    }
    catch (const nlohmann::detail::exception &e)
    {
        return false;
    }
    return add_person(personal_data(_person["name"], person_struct));
}
void database::remove_person(const std::string &_name)
{
    personal_info_map.erase(_name);
}
bool database::is_person_present(const std::string &_name) const
{
    return (personal_info_map.end() != personal_info_map.find(_name));
}
personal_data *database::get_person(const std::string &_name)
{
    if (is_person_present(_name))
        return personal_info_map[_name];
    return nullptr;
}

//account
bool database::add_account(account_data _acct)
{
    auto map_ptr = account_info_map.find(_acct.get_account_id());
    if (map_ptr == account_info_map.end())
    {
        account_info.push_back(_acct);
        account_info_map[_acct.get_account_id()] = &account_info.back();
    }
    else
    {
        map_ptr->second->set_data(_acct.get_data());
    }
    return true;
}
bool database::add_account(const std::string &_person, const json &_acct)
{
    account_data_struct acct_struct;

    try
    {
        if (!is_acct(_acct))
        {
            return false;
        }
        acct_struct.name = _person;
        acct_struct.bank = _acct["bank"];
        acct_struct.balance = _acct["balance"];
        acct_struct.currency = _acct["currency"];
    }
    catch (const nlohmann::detail::exception &e)
    {
        return false;
    }

    return add_account(account_data(_acct["account id"], acct_struct));
}
void database::remove_account(const std::string &_account_id)
{
    account_info_map.erase(_account_id);
}
bool database::is_account_present(const std::string &_account_id) const
{
    return (account_info_map.find(_account_id) != account_info_map.end());
}
account_data *database::get_account(const std::string &_account_id)
{
    if (is_account_present(_account_id))
    {
        return account_info_map[_account_id];
    }
    return nullptr;
}