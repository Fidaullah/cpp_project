#include "main_code.h"

#include <unistd.h>

#include "filereader.h"
#include "nlohmann/json.hpp"
#include "database.h"
#include "json_helper.h"
using json = nlohmann::json;

namespace emumba::training
{
    database main_db;

    const std::string db_file_name = "data/personal_data.json";
    const std::string query_file_name = "data/query.txt";
    const std::string result_file_name = "data/query_result.json";
    const std::string list_name = "Account Holders Info";

    bool initialize_data_base()
    {

        filereader db_file(db_file_name);
        json json_list = db_file.readfile();

        // checks if error ouccred during reading json
        if (is_error(json_list))
        {
            return false;
        }
        // checks if list exists
        if (json_list.end() == json_list.find(list_name))
        {
            return false;
        }

        main_db.parsedata(json_list[list_name]);

        return true;
    }
    void poll_queries()
    {
        filereader query_file(query_file_name);
        filereader result_file(result_file_name);
        json current_query = query_file.readline();
        json last_query = json{{"key", "value"}};
        while (true)
        {
            sleep(2); //sleep time to change query

            current_query = query_file.readline();
            if (current_query != last_query)
            {
                if (is_exit(current_query))
                {
                    break;
                }
                if (is_error(current_query))
                {
                    continue;
                }
                if (is_clear(current_query))
                {
                    result_file.writefile();
                    continue;
                }
                json result_json = main_db.query_function(current_query);

                if (!result_json.is_null())
                    result_file.appendfile(result_json);

                last_query = current_query;
            }
        }
    }
}