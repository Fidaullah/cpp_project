#include "json_helper.h"

#include <string>
#include <map>
#include <list>

using namespace emumba::training;
using json = nlohmann::json;

void emumba::training::to_json(json &_json, const personal_data &_person)
{
    _json = json{{"name", _person.get_name()},
                 {"age", _person.get_age()},
                 {"city", _person.get_city()},
                 {"coordinates", {{"lat", _person.get_coordinates()[LATI]}, {"long", _person.get_coordinates()[LONG]}}}};
}

void emumba::training::to_json(json &_json, const account_data &_acct)
{
    _json = json{{"name", _acct.get_name()},
                 {"account id", _acct.get_account_id()},
                 {"bank", _acct.get_bank()},
                 {"currency", _acct.get_currency()},
                 {"balance", _acct.get_balance()}};
}

bool almost_equal(const float &A, const float &B)
{
    float epsilon = 0.001;
    return fabs(A - B) < epsilon;
}

bool emumba::training::does_contains(per_pair _person, json _query)
{
    if (_person.second->get_city() == _query.begin().value())
        return true;
    if (_person.second->get_age() == _query.begin().value())
        return true;
    if (_query.begin().key() == "coordinates")
    {
        std::vector<float> cood = _person.second->get_coordinates();
        return almost_equal(cood[LATI], _query.begin().value()["lat"]) &&
               almost_equal(cood[LONG], _query.begin().value()["long"]);
    }
    if (_query.begin().key() == "lat")
    {
        return almost_equal( _query.begin().value(), _person.second->get_coordinates()[LATI]);
    }
    if (_query.begin().key() == "long")
    {
        return almost_equal( _query.begin().value(), _person.second->get_coordinates()[LONG]);
    }
    return false;
}
bool emumba::training::does_contains(acct_pair _acct, json _query)
{
    if (_acct.second->get_balance() == _query.begin().value())
        return true;
    if (_acct.second->get_bank() == _query.begin().value())
        return true;
    if (_acct.second->get_currency() == _query.begin().value())
        return true;
    return false;
}

bool emumba::training::is_person(const json &_person)
{
    auto iter_end = _person.end();
    if (_person.find("name") == iter_end)
    {
        return false;
    }
    if (_person.find("age") == iter_end)
    {
        return false;
    }
    if (_person.find("city") == iter_end)
    {
        return false;
    }
    if (_person.find("coordinates") == iter_end)
    {
        return false;
    }
    if (_person.find("coordinates").value().find("lat") == _person.find("coordinates").value().end())
    {
        return false;
    }
    if (_person.find("coordinates").value().find("long") == _person.find("coordinates").value().end())
    {
        return false;
    }
    if (_person.find("accounts") == iter_end)
    {
        return false;
    }

    return true;
}
bool emumba::training::is_acct(const json &_acct)
{
    auto iter_end = _acct.end();
    if (_acct.find("account id") == iter_end ||
        _acct.find("bank") == iter_end ||
        _acct.find("currency") == iter_end ||
        _acct.find("balance") == iter_end)
    {
        return false;
    }
    return true;
}

bool emumba::training::is_person_attr(const std::string &_key)
{
    std::list<std::string> person_attr({"city", "coordinates", "age", "lat", "long"});
    return find(person_attr.begin(), person_attr.end(), _key) != person_attr.end();
    
}
bool emumba::training::is_acct_attr(const std::string &_key)
{
    std::list<std::string> acct_attr({"bank", "balance", "name", "currency"});
    return find(acct_attr.begin(), acct_attr.end(), _key) != acct_attr.end();

}


bool emumba::training::is_exit(const json &_query)
{
    return _query.find("exit") != _query.end();
}
bool emumba::training::is_error(const json &_query)
{
    return _query.find("error") != _query.end();
}
bool emumba::training::is_clear(const json &_query)
{
    return _query.find("clear") != _query.end();
}
