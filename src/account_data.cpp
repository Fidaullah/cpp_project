#include "account_data.h"

using namespace emumba::training;

account_data::account_data(const std::string &_account_id, const account_data_struct &_acct_strcut)
{
    set_account_id(_account_id);
    set_data(_acct_strcut);
}

std::string account_data::get_account_id() const { return account_id; }
void account_data::set_account_id(const std::string &_account_id) { account_id = _account_id; }

account_data_struct account_data::get_data() const { return data; }
void account_data::set_data(const account_data_struct &_data) { data = _data; }
void account_data::set_data(const std::string &_name, const std::string &_bank,
                            const float &_balance, const std::string &_currency)
{
    set_name(_name);
    set_bank(_bank);
    set_balance(_balance);
    set_currency(_currency);
}

std::string account_data::get_name() const { return data.name; }
void account_data::set_name(const std::string &_name) { data.name = _name; }

std::string account_data::get_bank() const { return data.bank; }
void account_data::set_bank(const std::string &_bank) { data.bank = _bank; }

float account_data::get_balance() const { return data.balance; }
void account_data::set_balance(const float &_balance)
{
    data.balance = _balance;
}

std::string account_data::get_currency() const { return data.currency; }
void account_data::set_currency(const std::string &_currency)
{
    data.currency = _currency;
}
