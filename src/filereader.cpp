#include "filereader.h"

using namespace emumba::training;

filereader::filereader(const std::string &_file_name)
{
    //log if file already exists
    file_name = _file_name;
}

json filereader::readfile()
{
    json ret_json;
    file.open(file_name, std::ios::in);
    if (file.is_open())
    {
        try
        {
            ret_json = json::parse(file);
        }
        // exception handling
        catch (const nlohmann::detail::parse_error &e)
        {
            ret_json["error"] = e.what();
        }
        catch (const nlohmann::detail::type_error &e)
        {
            ret_json["error"] = e.what();
        }
    }
    else
    {
        //error statment
        ret_json["error"] = "file not open";
    }
    file.close();
    return ret_json;
}

json filereader::readline()
{
    json ret_json;
    std::string line;
    file.open(file_name, std::ios::in);
    if (file.is_open())
    {
        try
        {
            getline(file, line);
            ret_json = json::parse(line);
        }
        // exception handling
        catch (const nlohmann::detail::parse_error &e)
        {
            try
            {
                line = "{" + line + "}";
                ret_json = json::parse(line);
            }
            catch (const nlohmann::detail::parse_error &e)
            {
                ret_json["line"] = line;
                ret_json["error"] = e.what();
            }
        }
        catch (const nlohmann::detail::type_error &e)
        {
            ret_json["error"] = e.what();
        }
    }
    else
    {
        //error statment
        ret_json["error"] = "file not open";
    }
    file.close();
    return ret_json;
}

bool filereader::writefile()
{
    file.open(file_name, std::ios::out);
    bool ret_bool = file.is_open();
    file.close();
    return ret_bool;
}
bool filereader::writefile(const json &_data)
{
    bool ret_bool = true;
    file.open(file_name, std::ios::out);
    if (file.is_open())
    {
        file << _data.dump(5) <<std::endl;
    }
    else
    {
        //error statment
        ret_bool = false;
    }
    file.close();
    return ret_bool;
}
bool filereader::appendfile(const json &_data)
{
    bool ret_bool = true;
    file.open(file_name, std::ios::app);
    if (file.is_open())
    {
        file << _data.dump(5) << std::endl;
    }
    else
    {
        //error statment
        ret_bool = false;
    }
    file.close();
    return ret_bool;
}