#include "personal_data.h"

using namespace emumba::training;

personal_data::personal_data(const std::string &_name, const personal_data_struct &_person_struct)
{
    set_name(_name);
    set_data(_person_struct);
}
personal_data_struct personal_data::get_data() const { return data; }
void personal_data::set_data(const personal_data_struct &_data) { data = _data; }
void personal_data::set_data(const uint8_t &_age, const std::string &_city,
                             const std::vector<float> &_coordinates)
{
    set_age(_age);
    set_city(_city);
    set_coordinates(_coordinates);
}

std::string personal_data::get_name() const { return name; }
void personal_data::set_name(const std::string &_name) { name = _name; }

uint8_t personal_data::get_age() const { return data.age; }
void personal_data::set_age(const uint8_t &_age)
{
    data.age = _age;
}

std::string personal_data::get_city() const { return data.city; }
void personal_data::set_city(const std::string &_city) { data.city = _city; }

std::vector<float> personal_data::get_coordinates() const { return data.coordinates; }
void personal_data::set_coordinates(const std::vector<float> &_coordinates)
{

    data.coordinates = _coordinates;
}
void personal_data::set_coordinates(const float &_latitude, const float &_longitude)
{
    data.coordinates[0] = _latitude;
    data.coordinates[1] = _longitude;
}
