#include "main_code.h"

using namespace emumba::training;

int main()
{
    bool check = initialize_data_base();
    if (check)
    {
        poll_queries();
    }

    return 0;
}