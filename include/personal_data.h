#ifndef _PERSONAL_DATA_H_
#define _PERSONAL_DATA_H_

#include <string>
#include <vector>

#include "nlohmann/json.hpp"

namespace emumba::training
{
#define LATI 0
#define LONG 1

    struct personal_data_struct
    {
        uint8_t age;
        std::string city;
        std::vector<float> coordinates = std::vector<float>(2);
    };

    class personal_data
    {
    public:
        personal_data();
        personal_data(const std::string &_name, const personal_data_struct &_person_struct);

        personal_data_struct get_data() const;
        void set_data(const personal_data_struct &_data);
        void set_data(const uint8_t &_age, const std::string &_city,
                      const std::vector<float> &_coordinates);

        std::string get_name() const;
        void set_name(const std::string &_name);

        uint8_t get_age() const;
        void set_age(const uint8_t &_age);

        std::string get_city() const;
        void set_city(const std::string &_city);

        std::vector<float> get_coordinates() const;
        void set_coordinates(const std::vector<float> &_coordinates);
        void set_coordinates(const float &_latitude, const float &_longitude);

    private:
        std::string name;
        personal_data_struct data;
    };

}

#endif // _PERSONAL_DATA_H_