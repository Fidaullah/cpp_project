#ifndef _MAIN_CODE_H
#define _MAIN_CODE_H

namespace emumba::training
{
    bool initialize_data_base();
    void poll_queries();
}
#endif //_MAIN_CODE_H