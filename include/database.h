#ifndef _DATABASE_H_
#define _DATABASE_H_

#include <map>
#include <string>
#include <list>

#include "nlohmann/json.hpp"
#include "account_data.h"
#include "personal_data.h"
namespace emumba::training
{
    class database
    {
    public:
        //json related functions
        bool parsedata(nlohmann::json db_list);

        //query related
        nlohmann::json query_function(const nlohmann::json &_query_obj);

    private:
        std::map<std::string /*person's name*/, personal_data *> personal_info_map;
        std::map<std::string /*account id*/, account_data *> account_info_map;
        std::list<personal_data> personal_info;
        std::list<account_data> account_info;

        //query related
        void person_query(nlohmann::json &ret_json, const nlohmann::json &_query);
        void acct_query(nlohmann::json &ret_json, const nlohmann::json &_query);

        //person
        bool add_person(personal_data);
        bool add_person(const nlohmann::json &_person);
        void remove_person(const std::string &);
        bool is_person_present(const std::string &) const;
        personal_data *get_person(const std::string &);

        //account
        bool add_account(account_data);
        bool add_account(const std::string &_person, const nlohmann::json &_acct);
        void remove_account(const std::string &_account_id);
        bool is_account_present(const std::string &_account_id) const;
        account_data *get_account(const std::string &_account_id);
    };
}
#endif // _DATABASE_H_