#ifndef _ACCOUNT_DATA_H_
#define _ACCOUNT_DATA_H_

#include <string>

namespace emumba::training
{
    struct account_data_struct
    {
        std::string name;
        std::string bank;
        float balance;
        std::string currency;
    };

    class account_data
    {
    public:
        account_data(const std::string &_account_id, const account_data_struct &_acct_strcut);

        account_data_struct get_data() const;
        void set_data(const account_data_struct &_data);
        void set_data(const std::string &_name, const std::string &_bank,
                      const float &_balance, const std::string &_currency);

        std::string get_account_id() const;
        void set_account_id(const std::string &_account_id);

        std::string get_name() const;
        void set_name(const std::string &_name);

        std::string get_bank() const;
        void set_bank(const std::string &_bank);

        float get_balance() const;
        void set_balance(const float &_balance);

        std::string get_currency() const;
        void set_currency(const std::string &_currency);

    private:
        std::string account_id;
        account_data_struct data;
    };
}

#endif // _ACCOUNT_DATA_H_
