#ifndef _JSON_HELPER_H_
#define _JSON_HELPER_H_

#include "nlohmann/json.hpp"
#include "account_data.h"
#include "personal_data.h"

namespace emumba::training
{
    using per_pair = std::pair<std::string /*person's name*/, personal_data *>;
    using acct_pair = std::pair<std::string /*person's name*/, account_data *>;

    bool does_contains(per_pair _person, nlohmann::json _query);
    bool does_contains(acct_pair _acct, nlohmann::json _query);

    void to_json(nlohmann::json &_json, const personal_data &_person);
    void to_json(nlohmann::json &_json, const account_data &_acct);

    bool is_person(const nlohmann::json &_person);
    bool is_acct(const nlohmann::json &_acct);

    bool is_acct_attr(const std::string &_key);
    bool is_person_attr(const std::string &_key);

    bool is_clear(const nlohmann::json &_query);
    bool is_exit(const nlohmann::json &_query);
    bool is_error(const nlohmann::json &_query);

}
#endif // _JSON_HELPER_H_
