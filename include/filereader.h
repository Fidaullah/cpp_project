#ifndef _FILEREADER_H_
#define _FILEREADER_H_

#include <string>
#include <fstream>
#include "nlohmann/json.hpp"

using json = nlohmann::json;

namespace emumba::training
{
    class filereader
    {
    public:
        filereader(const std::string &_file_name);
        json readfile();
        json readline();
        bool writefile();
        bool writefile(const json &_data);
        bool appendfile(const json &_data);

    private:
        std::string file_name;
        std::fstream file;
    };

}
#endif //_FILEREADER_H_