cmake_minimum_required(VERSION 3.1)

project(benchmark)

add_executable(${PROJECT_NAME} mybenchmark.cpp)
set (EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR})

