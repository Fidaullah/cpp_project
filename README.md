# cpp_project

This is CPP training project for fall 2021 


## Getting started

To get this project use following command
```bash 
git clone https://gitlab.com/Fidaullah/cpp_project.git 
```

build dependencies
```bash 
sudo apt install cmake
```

To build and use this project 
```bash
cd (cpp_project)
mkdir build
cd build
cmake ..
make
./main
```

To build test
```bash
git checkout gtesting
#then build again
```

To build benchmark
```bash
git checkout benchmark
# or
git checkout benchmark_unordered
#then build again
```

## Project Overview

this is basic database management and query handling project. database stores to types of objects persons_data and account_data. At start of program data is get from **"./data/personal_data.json"**. input for query is set to **"./data/query.txt"**. And output of query is appended to **"./data/query_result.json"**
